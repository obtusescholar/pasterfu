### v 0.8
- Database moved
  - Linux: ~/.config/pasterfu/db.json
  - Windows: %USERPROFILE%/Documents/pasterfu/db.json
- Rewritten again
  - cli usage simplified
  - GUI much more likely
- Stdin input

### v 0.7
- Rewrote libfu and cli
  - Separated api file from library and cli
  - Trying to make code that would be easier to maintain
- Changed variable marking into '%link' style
- Added pyperclip to requirements
  - Use %clip variable for copying link

### v 0.6
- Database editing
- Support for custom databases
- Commandline arguments handling
- Completly rewrote library
- Using automatic script creation to make executables
