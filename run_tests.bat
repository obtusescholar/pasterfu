@echo py.exe -m flake8 pasterfu\
@py.exe -m flake8 pasterfu\ && @echo OK
@echo.

@echo py.exe -m flake8 --ignore=E402 tests\
@py.exe -m flake8 --ignore=E402 tests\ && @echo OK
@echo.

REM @echo py.exe -m unittest discover tests
REM @py.exe -m unittest discover tests
REM @echo.

@echo py.exe -m pytest tests\
@py.exe -m pytest tests\

@pause
