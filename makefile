SRC = pasterfu
PYTHON = python3

ifeq ($(PREFIX),)
	PREFIX := /opt
endif

FILES = CHANGELOG.md LICENSE README.md makefile
ALLFILES = $(SRC) $(FILES)

VER = $$(sed -n "/__version__/p" ${SRC}/constants.py | cut -d"'" -f2)

.PHONY = clean-py clean-build test run version tarball pytest


clean-pyc:
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	rm --force --recursive ${SRC}/__pycache__
	@printf "\n"

clean-build:
	rm --force --recursive build/
	rm --force --recursive dist/
	rm --force --recursive *.egg-info
	@printf "\n"

pytest:
	@pytest $(SRC) -v

test:
	pipenv check
	@printf "\nflake8\n"
	@$(PYTHON) -m flake8 --exclude="setup.py, test_*" $(SRC) && printf "OK\n\n"
	@pytest $(SRC) -v

build: clean-build clean-pyc test
	@printf "\n"
	${PYTHON} setup.py sdist bdist_wheel
	@printf "\nPyPI Test\n$(PYTHON) -m twine upload --repository testpypi dist/*"
	@printf "\nPyPI\n$(PYTHON) -m twine upload dist/*"

run:
	@${PYTHON} -m ${SRC}.${SRC}

version:
	@printf ${VER}

tarball:
	@printf "\n"
	tar -czvf bin/tar/$(SRC)-$(VER).tar.gz $(ALLFILES)
